var snbackup, fnbackup, grbackup, uidbackup;
var delBtnPressed;
var counter = 0;

$(document).ready(function() {

  $.getJSON("http://localhost:5000/getdata", function(data) {
    var json = data["data"];
    for (var k in json) {
      var newRow = $("<tr id=" + k + ">");
      var cols = "";

      cols += '<td style="text-align: right;"><p type="text" id="rank' + counter + '">' + (counter + 1) + '</p></td>';
      cols += '<td><p type="text" id="surname' + counter + '">' + json[k].surname + '</p></td>';
      cols += '<td><p type="text" id="firstname' + counter + '">' + json[k].firstname + '</p></td>';
      cols += '<td><p type="text" id="group' + counter + '">' + json[k].group + '</p></td>';
      cols += '<td><p type="text" id="uid' + counter + '">' + json[k].uid + '</p></td>';

      cols += '<td><div><button class="ibtnEdit btn btn-md"><span class="oi oi-copywriting"></span>';
      cols += '<button class="ibtnDelete btn btn-md" data-toggle="modal" data-target="#deleteModal"><span class="oi oi-trash"></span></div></td>';
      cols += '</tr>'
      newRow.append(cols);
      $("table.order-list").append(newRow);
      counter++;
    }
  });

  $("table.order-list").on("click", ".ibtnDelete", function(event) {
    delBtnPressed = $(this).closest("tr");
    var closestTR = $(this).closest("tr")[0];
    var sn = closestTR.cells[0].textContent;
    var fn = closestTR.cells[1].textContent;
    document.getElementById("modalDelMsg").innerHTML = '<p id="modalDelMsg">Are you sure you want to delete the user ' + sn + ' ' + fn + '?</p>';
  });

  $("div.delete-modal").on("click", ".ibtnDeleteConfirm", function(event) {
    deleteUserPOST(delBtnPressed);
  });

  $("#addUserModal").on("click", ".ibtnAddUser", function(event) {
    addUserPOST(this);
  });

  $("table.order-list").on("click", ".ibtnEdit", function(event) {
    var closestTR = $(this).closest("tr")[0];
    var id = $(this).closest('tr').attr('id');
    var sn = closestTR.cells[1].textContent;
    var fn = closestTR.cells[2].textContent;
    var gr = closestTR.cells[3].textContent;
    var uid = closestTR.cells[4].textContent;
    snbackup = closestTR.cells[1].innerHTML;
    fnbackup = closestTR.cells[2].innerHTML;
    grbackup = closestTR.cells[3].innerHTML;
    uidbackup = closestTR.cells[4].innerHTML;

    closestTR.cells[1].innerHTML = '<td><input type="text" class="form-control" id="inputSurname' + id + '" value="' + sn + '"/></td>';
    closestTR.cells[2].innerHTML = '<td><input type="text" class="form-control" id="inputFirstname' + id + '" value="' + fn + '"/></td>';
    var html2 = '<td><select id="groupSelect' + id + '" class="form-control">';
    html2 += '<option>1: Full Access</option>';
    html2 += '<option>2: Normal</option>';
    html2 += '<option>3: Special</option>';
    html2 += '<option>4: Invalid</option>';
    html2 += '</select></td>';
    closestTR.cells[3].innerHTML = html2;
    document.getElementById("groupSelect" + id).value = document.getElementById("groupSelect" + id)[gr - 1].innerHTML;

    closestTR.cells[4].innerHTML = '<td><button class="uidBtn btn btn-md" data-toggle="modal" data-target="#uidReadModal" id="uidButton' + id + '">' + uid + '</button></td>'

    var btns = '<td><div><button class="ibtnCheck btn btn-md"><span class="oi oi-check"></span>';
    btns += '<button class="ibtnCancel btn btn-md"><span class="oi oi-x"></span></div></td>';
    closestTR.cells[5].innerHTML = btns;
  });

  $("table.order-list").on("click", ".uidBtn", function(event) {
    readUIDReq(this);
    var shit = "shit";
  });

  $("table.order-list").on("click", ".ibtnCheck", function(event) {
    var closestTR = $(this).closest("tr")[0];
    var id = $(this).closest('tr').attr('id');

    var sn = document.getElementById('inputSurname' + id).value;
    var fn = document.getElementById('inputFirstname' + id).value;
    var gr = document.getElementById('groupSelect' + id).selectedIndex + 1;
    var uid = document.getElementById('uidButton' + id).childNodes[0].nodeValue;

    closestTR.cells[1].innerHTML = '<td><p type="text" id="surname' + id + '">' + sn + '</p></td>';
    closestTR.cells[2].innerHTML = '<td><p type="text" id="firstname' + id + '"/>' + fn + '</td>';
    closestTR.cells[3].innerHTML = '<td><p type="text" id="group' + id + '"/>' + gr + '</td>';
    closestTR.cells[4].innerHTML = '<td><p type="text" id="uid' + id + '">' + uid + '</p></td>';

    var btns = '<td><div><button class="ibtnEdit btn btn-md"><span class="oi oi-copywriting"></span>';
    btns += '<button class="ibtnDelete btn btn-md"><span class="oi oi-trash"></span></div></td>';
    closestTR.cells[5].innerHTML = btns;
  });

  $("table.order-list").on("click", ".ibtnCancel", function(event) {
    var closestTR = $(this).closest("tr")[0];
    var id = $(this).closest('tr').attr('id');
    closestTR.cells[1].innerHTML = snbackup;
    closestTR.cells[2].innerHTML = fnbackup;
    closestTR.cells[3].innerHTML = grbackup;
    closestTR.cells[4].innerHTML = uidbackup;

    var btns = '<td><div><button class="ibtnEdit btn btn-md"><span class="oi oi-copywriting"></span>';
    btns += '<button class="ibtnDelete btn btn-md" data-toggle="modal" data-target="#deleteModal"><span class="oi oi-trash"></span></div></td>';
    closestTR.cells[5].innerHTML = btns;
  });

  $(("div#addUserModal")).on("click", "#newUserUID", function(event) {
    $("#newUserUIDAnimation").removeClass("hidden");
    $("#newUserUID").addClass("hidden");
    readUIDReq(this, $("#newUserUIDAnimation"));
  });

});

function searchTable() {
  var input, filter, table, tr, sn, fn, uid, i, display;
  input = document.getElementById("searchBar");
  filter = input.value.toUpperCase();
  table = document.getElementById("userInfo");
  tr = table.getElementsByTagName("tr");
  for (i = 1; i < tr.length; i++) {
    sn = tr[i].getElementsByTagName("td")[1];
    fn = tr[i].getElementsByTagName("td")[2];
    uid = tr[i].getElementsByTagName("td")[4];
    display = false;

    if (sn && sn.innerHTML.toUpperCase().indexOf(filter) > -1) display = true;

    if (fn && fn.innerHTML.toUpperCase().indexOf(filter) > -1) display = true;
    else display |= false;

    if (uid && uid.innerHTML.toUpperCase().indexOf(filter) > -1) display = true;
    else display |= false;

    if (display) tr[i].style.display = "";
    else tr[i].style.display = "none";
  }
}

function readUIDReq(btnPressed, animation) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      var data = JSON.parse(xmlHttp.responseText);
      if ($('#uidReadModal').is(':visible')) {
        $('#uidReadModal').modal('hide');
        btnPressed.innerHTML = data["uid"];
      } else if ($('#addUserModal').is(':visible')) {
        btnPressed.innerHTML = data["uid"];
        $("#newUserUIDAnimation").addClass("hidden");
        $("#newUserUID").removeClass("hidden");

      }
    }
  }
  $(".ibtnReadCancel").on("click", function(event) {
    xmlHttp.abort();
  });
  xmlHttp.open("GET", "http://localhost:5000/read", true);
  xmlHttp.send(null);
}

function deleteUserPOST(btnPressed) {
  var probablyUID = delBtnPressed.closest("tr")[0].cells[4].innerText;
  var sendData = JSON.stringify({
    "uid": probablyUID
  });

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      var data = JSON.parse(xmlHttp.responseText);
      if (data["result"] == "Success") {
        console.log("Success");
        delBtnPressed.remove();
        counter -= 1
      } else if (data["result"] == "Error") {
        console.log("Error");
        xmlHttp.open("POST", "http://localhost:5000/delete", true);
        xmlHttp.setRequestHeader("Content-type", "application/json");
        xmlHttp.send(sendData);
      }
    }
  }
  xmlHttp.open("POST", "http://localhost:5000/delete", true);
  xmlHttp.setRequestHeader("Content-type", "application/json");
  xmlHttp.send(sendData);
}

function addUserPOST(btnPressed) {
  var sn = document.getElementById("newUserSurname").value;
  var fn = document.getElementById("newUserFirstname").value;
  var gr = document.getElementById("newUserGroup").selectedIndex + 1;
  var uid = document.getElementById("newUserUID").innerHTML;
  var sendData = JSON.stringify({
    "surname": sn,
    "firstname": fn,
    "group": gr,
    "uid": uid
  });

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      var data = JSON.parse(xmlHttp.responseText);
      if (data["result"] == "Success") {
        console.log("Success");
        var newRow = $("<tr id=" + counter + ">");
        var cols = "";

        cols += '<td style="text-align: right;"><p type="text" id="rank' + counter + '">' + (counter + 1) + '</p></td>';
        cols += '<td><p type="text" id="surname' + counter + '">' + sn + '</p></td>';
        cols += '<td><p type="text" id="firstname' + counter + '">' + fn + '</p></td>';
        cols += '<td><p type="text" id="group' + counter + '">' + gr + '</p></td>';
        cols += '<td><p type="text" id="uid' + counter + '">' + uid + '</p></td>';

        cols += '<td><div><button class="ibtnEdit btn btn-md"><span class="oi oi-copywriting"></span>';
        cols += '<button class="ibtnDelete btn btn-md" data-toggle="modal" data-target="#deleteModal"><span class="oi oi-trash"></span></div></td>';
        cols += '</tr>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
        $('#addUserModal').modal('hide');
      } else if (data["result"] == "Error") {
        console.log("Error");
      } else {
        console.log("Sth else happened!");
      }
    }
  }
  xmlHttp.open("POST", "http://localhost:5000/insert", true);
  xmlHttp.setRequestHeader("Content-type", "application/json");
  xmlHttp.send(sendData);
}