clc;
clear *;
close all;

sigma = 10;
[img, map, alpha] = imread("background_sucuk_doener.jpg");
blur = imsmooth(img, "Gaussian", sigma);
subplot(1,2,1);
imshow(img);
subplot(1,2,2);
imshow(blur);
imwrite(blur, "background_blur_sucuk_doener.jpg");